#ifndef MYCHARTTWO_H
#define MYCHARTTWO_H

#include <QtCharts>
QT_CHARTS_USE_NAMESPACE

class MyChartTwo : public QChartView
{
public:
    MyChartTwo();

public:
    QChart *chart;           //图表的画布
    QLineSeries *series0;  //序列线
    QLineSeries* series1;  //序列线1
    QValueAxis *axisX;       //X轴
    QValueAxis *axisY;       //左Y轴
    int flag = 0;            //标识场景
    double dt = 0;
    double Vs = 0.0;
    double Vd = 0.0;
    double CMR = 0.0;
    bool isPos = true;
    double E = 0.0;
    bool isUp = true;
    double B = 0.0;
    bool isOut = true;
    double layer = 0.0;

public:
    void initChart();       //初始化图表
    void initDataMem();     //初始化数据成员
    void updateChart();     //更新图表

};

#endif // MYCHARTTWO_H
