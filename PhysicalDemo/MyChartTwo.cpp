#include "MyChartTwo.h"
#include "Calculate.h"
#include "math.h"

MyChartTwo::MyChartTwo()
{
    initDataMem();//初始化数据成员
    initChart();//初始化图表
}

void MyChartTwo::initChart()
{
    //设置图表标题并隐藏legend
    chart->setTitle("电场中粒子运动θ-T示意图");
    QFont fonttitle;
    fonttitle.setFamily("隶书");
    fonttitle.setPointSize(12);
    fonttitle.setPixelSize(24);
    chart->setTitleFont(fonttitle);
    chart->legend()->hide();

    //设置坐标字体
    QFont font;
    font.setFamily("黑体");
    font.setPointSize(9);
    font.setPixelSize(18);


    //配置各个坐标轴
    axisX->setRange(0, 71);
    axisX->setTickCount(5);
    axisX->setLabelsFont(font);
    axisX->setTitleText("时间T(x10^-9.5 s)");
    axisX->setTitleFont(font);

    axisY->setRange(-90, 90);
    axisY->setTickCount(5);
    axisY->setLabelsFont(font);
    axisY->setTitleText("角度θ(°)");
    axisY->setTitleFont(font);

    //将各个坐标轴添加到图表上
    chart->addAxis(axisX, Qt::AlignBottom);          // 将X轴添加到图表上
    chart->addAxis(axisY, Qt::AlignLeft);            // 将Y轴添加到图表上

    //将曲线序列添加到图表上
    chart->addSeries(series0);                        // 将曲线对象添加到图表上
    chart->addSeries(series1);                       // 添加曲线0

    //为图表设置动画
    chart->setAnimationOptions(QChart::SeriesAnimations);

    //将曲线序列和对应的坐标轴关联
    series0->attachAxis(axisX);
    series1->attachAxis(axisX);
    series0->attachAxis(axisY);
    series1->attachAxis(axisY);

    //设置序列点可见
    series0->setPointsVisible(true);
    series1->setPointsVisible(true);

}

void MyChartTwo::initDataMem()
{
    chart = new QChart();             //创建图表
    series0 = new QSplineSeries ();    //创建序列
    series1 = new QSplineSeries();     //创建序列1
    axisX = new QValueAxis();         //创建X轴
    axisY = new QValueAxis();         //创建左Y轴

}

void MyChartTwo::updateChart()
{
    dt += 1;
    if(flag == 0)
    {
        static int i = 1;
        double A = inEleCalA(pow(10, -9.55) * dt, Vs, Vd, CMR, isPos, E, isUp);
        series0->append(dt, A);

    }
    else if(flag == 1)
    {
        double A = inMagCalA(dt * pow(10, -8.5), Vs, Vd, CMR, isPos, B, isOut);
        series0->append(dt, A);
    }
    else
    {


        double Vxt = inEleAndMagCalVx(dt * pow(10, -11), Vs, CMR, isPos, E, isUp, B, isOut);
        double Vx = Vxt /  pow(10, this->layer);

        series0->append(dt, abs(Vx));
        double Vyt = inEleAndMagCalVy(dt * pow(10, -11), Vs, CMR, isPos, E, isUp, B, isOut);

        double Vy = Vyt /  pow(10, this->layer);;
        series1->append(dt, abs(Vy));


    }

}
