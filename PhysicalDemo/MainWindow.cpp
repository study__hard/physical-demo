#include "MainWindow.h"
#include <QtCharts>
QT_CHARTS_USE_NAMESPACE
#include "ui_MainWindow.h"
#include <QPainter>
#include <QTimer>
#include "Calculate.h"
#include "math.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initDataMem();                  //初始化数据成员
    initUI();                       //初始化界面
    initConnect();                  //初始化连接
    initParameterSet();             //初始化参数设置
    disableButton();                //禁用一些按钮
    defaultIntoEle();               //默认进入电场
    setZeroAndNoenable("0", false); //置0和禁止输入
    //限制初速度的入射角度
    ui->speedDirection->setValidator(new QIntValidator(-90, 90, this));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initUI()
{
    //设置窗口标题
    //this->setWindowTitle("带电粒子在电磁场中运动的仿真实验");

    //设置速度方向输入框的默认提示
    ui->speedDirection->setPlaceholderText("与X轴正向夹角");

    //把图表一添加到界面中
    ui->myChartOne->setChart(myChartOne->chart);

    //把图表二添加到界面中
    ui->myChartTwo->setChart(myChartTwo->chart);

    //设置抗锯齿
    ui->myChartOne->setRenderHint(QPainter::Antialiasing);
    ui->myChartTwo->setRenderHint(QPainter::Antialiasing);

    //隐藏到两个tabwidget的tab页
    ui->plottingArea->tabBar()->hide();
    ui->valueDisplay->tabBar()->hide();

    QFont font;
    font.setPixelSize(20);
    ui->actEle->setFont(font);
    ui->actMag->setFont(font);
    ui->actnEleMag->setFont(font);
    ui->actRet->setFont(font);
}

void MainWindow::initDataMem()
{
    timer = new QTimer(this);              //创建定时器
    dt = 0;                                //同步时间
    myChartOne = new MyChartOne();         //创建图表一
    myChartTwo = new MyChartTwo();         //创建图表二
}

void MainWindow::initConnect()
{
    connect(timer, &QTimer::timeout, this, [=]()//时间到了拿到设置的数据更新绘图
    {
        if(ui->plottingArea->currentIndex() == 0)
        {
            if(ui->eleScence->isRepaint)
            {
                emitAgain();
                ui->eleScence->isRepaint = false;
            }
        }
        else if(ui->plottingArea->currentIndex() == 1)
        {
            if(ui->magScence->isRepaint)
            {
                emitAgain();
                ui->magScence->isRepaint = false;
            }
        }
        else
        {
            if (ui->eleAndMagScence->isRepaint)
            {
                emitAgain();
                ui->eleAndMagScence->isRepaint = false;
            }
        }





        dt += 1;

        myChartTwo->updateChart();
        myChartOne->updateChart();

        setValue(dt, ui->valueDisplay->currentIndex());

        switch (ui->plottingArea->currentIndex()) {//该switch用于绘图，isDraw是绘图的判断位
        case 0:
            ui->eleScence->isDraw = true;
            ui->plottingArea->currentWidget()->repaint();
            break;
        case 1:
            ui->magScence->isDraw = true;
            ui->plottingArea->currentWidget()->repaint();
            break;
        default:
            ui->eleAndMagScence->isDraw = true;
            ui->plottingArea->currentWidget()->repaint();
            break;
        }
    });
}

void MainWindow::initParameterSet()
{
    ui->speedSize->setText(QString("3.0x10^7"));
    ui->speedDirection->setText(QString("0"));
    ui->CMR->setText(QString("1.758x10^11"));
    ui->isPosCharge->setChecked(true);

}

void MainWindow::setScenceParameter(int index)
{
    if(index == 0)
    {
        myChartOne->Vs = sciNotation(ui->speedSize->text());
        myChartTwo->Vs = sciNotation(ui->speedSize->text());

        myChartOne->Vd = sciNotation(ui->speedDirection->text());
        myChartTwo->Vd = sciNotation(ui->speedDirection->text());

        myChartOne->CMR = sciNotation(ui->CMR->text());
        myChartTwo->CMR = sciNotation(ui->CMR->text());

        if(ui->isPosCharge->isChecked())
        {
            myChartOne->isPos = true;
            myChartTwo->isPos = true;
        }
        else if(ui->isNagCharge->isChecked())
        {
            myChartOne->isPos = false;
            myChartTwo->isPos = false;
        }

        myChartOne->E = sciNotation(ui->eleSize->text());
        myChartTwo->E = sciNotation(ui->eleSize->text());

        if(ui->up->isChecked())
        {
            myChartOne->isUp = true;
            myChartTwo->isUp = true;
        }
        else if(ui->down->isChecked())
        {
             myChartOne->isUp = false;
             myChartTwo->isUp = false;
        }

    }
    else if(index == 1)
    {
        myChartOne->Vs = sciNotation(ui->speedSize->text());
        myChartTwo->Vs = sciNotation(ui->speedSize->text());

        myChartOne->Vd = sciNotation(ui->speedDirection->text());
        myChartTwo->Vd = sciNotation(ui->speedDirection->text());

        myChartOne->CMR = sciNotation(ui->CMR->text());
        myChartTwo->CMR = sciNotation(ui->CMR->text());

        if(ui->isPosCharge->isChecked())
        {
            myChartOne->isPos = true;
            myChartTwo->isPos = true;
        }
        else if(ui->isNagCharge->isChecked())
        {
            myChartOne->isPos = false;
            myChartTwo->isPos = false;
        }

        myChartOne->B = sciNotation(ui->magSize->text());
        myChartTwo->B = sciNotation(ui->magSize->text());

        if(ui->isOut->isChecked())
        {
            myChartOne->isOut = true;
            myChartTwo->isOut = true;
        }
        else if(ui->isInward->isChecked())
        {
             myChartOne->isOut = false;
             myChartTwo->isOut = false;
        }

    }
    else
    {
        myChartOne->Vs = sciNotation(ui->speedSize->text());
        myChartTwo->Vs = sciNotation(ui->speedSize->text());

        myChartOne->Vd = sciNotation(ui->speedDirection->text());
        myChartTwo->Vd = sciNotation(ui->speedDirection->text());

        myChartOne->CMR = sciNotation(ui->CMR->text());
        myChartTwo->CMR = sciNotation(ui->CMR->text());

        if(ui->isPosCharge->isChecked())
        {
            myChartOne->isPos = true;
            myChartTwo->isPos = true;
        }
        else if(ui->isNagCharge->isChecked())
        {
            myChartOne->isPos = false;
            myChartTwo->isPos = false;
        }

        myChartOne->E = sciNotation(ui->eleSize->text());
        myChartTwo->E = sciNotation(ui->eleSize->text());

        if(ui->up->isChecked())
        {
             myChartOne->isUp = true;
             myChartTwo->isUp = true;
        }
        else if(ui->down->isChecked())
        {
             myChartOne->isUp = false;
             myChartTwo->isUp = false;
        }

        myChartOne->B = sciNotation((ui->magSize->text()));
        myChartTwo->B = sciNotation((ui->magSize->text()));

        if(ui->isOut->isChecked())
        {
             myChartOne->isOut = true;
             myChartTwo->isOut = true;
        }
        else if(ui->isInward->isChecked())
        {
             myChartOne->isOut = false;
             myChartTwo->isOut = false;
        }

    }

}

void MainWindow::defaultIntoEle()
{
    QFont font;
    font.setPixelSize(16);
    ui->speedSize->setText(QString("3.0x10^7"));
    ui->speedSize->setFont(font);
    ui->speedDirection->setText(QString("0"));
    ui->speedDirection->setFont(font);
    ui->isNagCharge->setChecked(true);
    ui->CMR->setText("1.758x10^11");
    ui->centralWidget->setFont(font);
    ui->eleSize->setText("1.0x10^4");
    ui->eleSize->setFont(font);
    ui->plottingArea->setCurrentIndex(0);
    ui->up->setChecked(true);
    ui->eleSize->setEnabled(true);
    ui->up->setEnabled(true);
    ui->down->setEnabled(true);
    ui->magSize->setEnabled(false);
    ui->isInward->setEnabled(false);
    ui->isOut->setEnabled(false);
}

void MainWindow::disableButton()
{
    QFont font;
    font.setPixelSize(20);
    ui->actStart->setEnabled(true);
    ui->actStart->setFont(font);
    ui->actAgain->setEnabled(false);
    ui->actAgain->setFont(font);
    ui->actGo->setEnabled(false);
    ui->actGo->setFont(font);
    ui->actStop->setEnabled(false);
    ui->actStop->setFont(font);
}

void MainWindow::disableAlls(bool flag)
{
    ui->speedSize->setEnabled(flag);
    ui->speedDirection->setEnabled(flag);
    ui->CMR->setEnabled(flag);
    ui->isNagCharge->setEnabled(flag);
    ui->isPosCharge->setEnabled(flag);
    ui->up->setEnabled(flag);
    ui->down->setEnabled(flag);
    ui->eleSize->setEnabled(flag);
    ui->magSize->setEnabled(flag);
    ui->isInward->setEnabled(flag);
    ui->isPosCharge->setEnabled(flag);
    ui->isOut->setEnabled(flag);
}

void MainWindow::openSomeSettings(int index)
{
    if(index == 0)
    {
        //打开电场配置关闭磁场配置
        ui->eleSize->setEnabled(true);
        ui->up->setEnabled(true);
        ui->down->setEnabled(true);
        ui->magSize->setEnabled(false);
        ui->isInward->setEnabled(false);
        ui->isOut->setEnabled(false);

    }
    else if(index == 1)
    {
        //打开磁场配置关闭电场配置
        ui->magSize->setEnabled(true);
        ui->isInward->setEnabled(true);
        ui->isOut->setEnabled(true);
        ui->eleSize->setEnabled(false);
        ui->up->setEnabled(false);
        ui->down->setEnabled(false);
    }
    else
    {
        //打开电磁场的配置
        ui->magSize->setEnabled(true);
        ui->eleSize->setEnabled(true);
        ui->up->setEnabled(true);
        ui->down->setEnabled(true);
        ui->isInward->setEnabled(true);
        ui->isOut->setEnabled(true);
    }

    //打开粒子设置
    ui->speedSize->setEnabled(true);
    ui->speedDirection->setEnabled(true);
    ui->CMR->setEnabled(true);
    ui->isNagCharge->setEnabled(true);
    ui->isPosCharge->setEnabled(true);
}

void MainWindow::clearPlottingArea()
{
    int index = ui->plottingArea->currentIndex();// 0电 1磁 2 电磁
    if(index == 0)
    {
        ui->eleScence->times = 0;
        ui->eleScence->dt = 0;
        ui->eleScence->repaint();
    }
    else if(index == 1)
    {
        ui->magScence->times = 0;
        ui->magScence->dt = 0;
        ui->magScence->repaint();
    }
    else
    {
        ui->eleAndMagScence->times = 0;
        ui->eleAndMagScence->dt = 0;
        ui->eleAndMagScence->repaint();
    }

}

void MainWindow::setValue(double dt, int index)
{

    QString stra = ui->speedSize->text();
    double v = sciNotation(stra);

    QString strb = ui->speedDirection->text();
    double angle = sciNotation(strb);

    QString strc = ui->CMR->text();
    double CMR = sciNotation(strc);

    QString strd = ui->eleSize->text();
    double E = sciNotation(strd);

    QString stre = ui->magSize->text();
    double B = sciNotation(stre);

    bool isPos;
    bool isUp;
    bool isOut;

    if(ui->isPosCharge->isChecked())//电场、电磁场中
    {
        isPos = true;
    }
    if(ui->isNagCharge->isChecked())
    {
        isPos = false;
    }
    if(ui->up->isChecked())
    {
        isUp = true;
    }
    if(ui->down->isChecked())
    {
        isUp = false;
    }


    if(ui->isOut->isChecked())//磁场、电磁场中
    {
        isOut = true;
    }
    if(ui->isInward->isChecked())
    {
        isOut = false;
    }


    if(index == 0)//为电场数据框设置数据
    {
        dt *= pow(10, -9.55);

        double X = inEleCalX(dt,v,angle);//调用函数，进行计算
        if(X < 1e-15)
        {
            X = 0;
        }


        double Y = inEleCalY(dt,v,angle,CMR,isPos,E,isUp);

        double V = inEleCalV(dt,v,angle,CMR,isPos,E,isUp);


        double Tan = inEleCalTan(dt,v,angle,CMR,isPos,E,isUp);

        QString str1 = sciResult(V);
        QString str2 = "";
        QString str3 = sciResult(Tan);
        QString str4 = "";

        if(ui->eleScence->times < ui->eleScence->data.size())
        {
            str2 = sciResult((double)((ui->eleScence->data[ui->eleScence->times].x())) / pow(10, 2.35) );
            str4 = sciResult((double)(-(ui->eleScence->data[ui->eleScence->times].y())) / pow(10, 2.25));
        }

        ui->v11->setText(QString(str1));//V
        ui->v12->setText(QString(str2));//X
        ui->v13->setText(QString(str3));//tan
        ui->v14->setText(QString(str4));//Y
    }


    else if(index == 1)//为磁场数据框设置数据
    {
        dt *= pow(10, -8);

        double X = inMagCalX(dt,v,angle,CMR,isPos,B,isOut);//调用函数，进行计算

        double Y = inMagCalY(dt,v,angle,CMR,isPos,B,isOut);

        //qDebug()<<"设置数据"<<dt<<ui->magScence->data[dt].x();
        double R = inMagCalR(v,CMR,B) - 0.001;
        double T = inMagCalT(CMR,B);

        QString str1 = sciResult(R);//调用函数，用科学计数法来表示结果
        QString str2 = "";
        QString str4 = "";
        if(ui->magScence->times < ui->magScence->data.size())
        {
            str2 = sciResult((double)(ui->magScence->data[ui->magScence->times].x()));
            str4 = sciResult((double)-(ui->magScence->data[ui->magScence->times].y()));
        }

        QString str3 = sciResult(T);

        ui->v21->setText(QString(str1));//R
        ui->v22->setText(QString(str2));//X
        ui->v23->setText(QString(str3));//T
        ui->v24->setText(QString(str4));//Y



    }

    else//为电磁场数据框设置数据
    {
        dt *= pow(10, -11);
        double X = inEleAndMagCalX(dt,v,CMR,isPos,E,isUp,B,isOut);//调用函数，进行计算
        double Y = inEleAndMagCalY(dt,v,CMR,isPos,E,isUp,B,isOut);
        double V = inEleAndMagCalV(dt,v,CMR,isPos,E,isUp,B,isOut);
        double A = inEleAndMagCalA(dt,v,CMR,isPos,E,isUp,B,isOut);

        QString str1 = sciResult(V);
        QString str2 = "";
        QString str3 = sciResult(A);
        QString str4 = "";

        if(ui->eleAndMagScence->times < ui->eleAndMagScence->data.size())
        {
            str2 = sciResult((double)(ui->eleAndMagScence->data[ui->eleAndMagScence->times].x()));
            str4 = sciResult((double)-(ui->eleAndMagScence->data[ui->eleAndMagScence->times].y()));
        }

        ui->v31->setText(QString(str1));//V
        ui->v32->setText(QString(str2));//X
        ui->v33->setText(QString(str3));//角度
        ui->v34->setText(QString(str4));//Y
    }

}

void MainWindow::setZeroAndNoenable(QString str = "0", bool flag = false)
{
    ui->v11->setText(str);
    ui->v12->setText(str);
    ui->v13->setText(str);
    ui->v14->setText(str);
    ui->v21->setText(str);
    ui->v22->setText(str);
    ui->v23->setText(str);
    ui->v24->setText(str);
    ui->v31->setText(str);
    ui->v32->setText(str);
    ui->v33->setText(str);
    ui->v34->setText(str);

    ui->v11->setEnabled(flag);
    ui->v12->setEnabled(flag);
    ui->v13->setEnabled(flag);
    ui->v14->setEnabled(flag);
    ui->v21->setEnabled(flag);
    ui->v22->setEnabled(flag);
    ui->v23->setEnabled(flag);
    ui->v24->setEnabled(flag);
    ui->v31->setEnabled(flag);
    ui->v32->setEnabled(flag);
    ui->v33->setEnabled(flag);
    ui->v34->setEnabled(flag);

}

void MainWindow::emitAgain()
{
    //显示数据框置0
    setZeroAndNoenable("0", false); //置0和禁止输入

    //时间t归0
    dt = 0;
    myChartOne->dt = 0;
    myChartTwo->dt = 0;

    //清楚绘图区
    clearPlottingArea();

    //清楚图表区
    myChartOne->series0->clear();
    myChartOne->series1->clear();
    myChartTwo->series1->clear();
    myChartTwo->series0->clear();


    //禁用||打开一些按钮
    ui->actAgain->setEnabled(true);
    ui->actStart->setEnabled(false);
    ui->actStop->setEnabled(true);
    ui->actGo->setEnabled(false);
}

void MainWindow::updateAxis(int index)
{
    if(index == 0)
    {
        //设置图表的X轴范围
        myChartOne->axisX->setRange(0, ui->eleScence->data.size() + 3);
        myChartTwo->axisX->setRange(0, ui->eleScence->data.size() + 3);

        //设置图表的Y轴范围
        double V0 =  sciNotation(ui->speedSize->text());
        double a = sciNotation(ui->CMR->text()) * sciNotation(ui->eleSize->text());
        double VMax =  V0 + a * (ui->eleScence->data.size() - 1) * pow(10, -9.5);
        myChartOne->size = ui->eleScence->data.size() - 1;

        myChartOne->layer = getNumLayer(sciResult(VMax)) - 1;

        myChartOne->axisY->setRange(0, VMax / pow(10, getNumLayer(sciResult(VMax)) - 1));
        myChartTwo->axisY->setRange(-90, 90);

        //设置图表的坐标轴标题
        myChartOne->axisY->setTitleText(QString("速率V(x10^%1 m/s)")
                                        .arg(getNumLayer(sciResult(VMax)) - 1));
        myChartOne->axisX->setTitleText(QString("时间T(x10^-9.5 s)"));
        myChartTwo->axisX->setTitleText(QString("时间T(x10^-9.5 s)"));


    }
    else if(index  == 1)
    {
        //设置图表的X轴范围
        myChartOne->axisX->setRange(0, ui->magScence->data.size() +3);
        myChartTwo->axisX->setRange(0, ui->magScence->data.size() +3);

        //设置图表的Y轴范围
        myChartTwo->axisY->setRange(-180, 180);

        //设置图表的坐标轴标题
        myChartOne->axisX->setTitleText(QString("时间T(x10^-8.5 s)"));
        myChartTwo->axisX->setTitleText(QString("时间T(x10^-8.5 s)"));

        myChartOne->axisY->setTitleText(QString("速率V(x10^%1 m/s)")
             .arg(getNumLayer(sciResult(sciNotation(ui->speedSize->text())))));
    }
    else
    {
        myChartOne->axisX->setRange(0, ui->eleAndMagScence->data.size() +3);
        myChartTwo->axisX->setRange(0, ui->eleAndMagScence->data.size() +3);

        //设置图表的左Y轴和右Y轴的范围
        double V = sciNotation(ui->speedSize->text());
        double CMR = sciNotation(ui->CMR->text());
        double E = sciNotation(ui->eleSize->text());
        double B = sciNotation(ui->magSize->text());
        bool isPost = (ui->isPosCharge->isChecked())? true : false;
        bool isUp = (ui->up->isChecked())? true : false;
        bool isOut = (ui->isOut->isChecked())? true : false;

        double V0 = E / B;      //开始所需要的平衡初速度
        double w = CMR * B;     //做圆周运动的角速度

        QVector<bool> temp;
        temp.push_back(isUp);
        temp.push_back(isOut);
        temp.push_back(isPost);

        int sum = 0;
        for(auto item : temp)      //二进制转换成十进制
        {
            sum = sum * 2 + item;
        }
        double Vxmax = 0.0;
        double Vymax = 0.0;
        double VMax = 0.0;

        if(sum == 5 || sum == 2)//枚举对应情况，写出相应的公式
        {

            Vxmax = 2 * V0 + V;

            Vymax = V + V0;

        }
        else if(sum == 7 || sum == 0)
        {

            Vxmax = std::max(abs(V - V0 + V0), abs(V0 - V + V0));

            Vymax = std::max(abs(V0 - V), abs(V - V0));
        }
        else if(sum == 1 || sum == 6)
        {

            Vxmax = std::max(abs(V - V0 + V0), abs(V0 - V + V0));

            Vymax = std::max(abs(V0 - V), abs(V - V0));
        }
        else
        {

            Vxmax = 2 * V0 + V;

            Vymax = V + V0;
        }
        double Vxymax = std::max(Vxmax, Vymax);


        VMax = sqrt(Vxmax * Vxmax + Vymax * Vymax);
        myChartOne->layer = getNumLayer(sciResult(VMax)) - 1;
        myChartTwo->layer = getNumLayer(sciResult(Vxymax)) - 1;

        myChartOne->axisY->setRange(0, (VMax) /
                                    pow(10, getNumLayer(sciResult(VMax)) - 1) + 3);

        myChartTwo->axisY->setRange(0, (Vxymax) /
                                    pow(10, getNumLayer(sciResult(Vxymax)) - 1) + 3);


        //设置图表的坐标轴标题
        myChartOne->axisY->setTitleText(QString("速率V(x10^%1 m/s)")
                                        .arg(getNumLayer(sciResult(VMax)) - 1));
        myChartTwo->axisY->setTitleText(QString("速率Vx/Vy(x10^%1 m/s)")
                                        .arg(getNumLayer(sciResult(Vxymax)) - 1));

        myChartOne->axisX->setTitleText(QString("时间T(x10^-11 s)"));
        myChartTwo->axisX->setTitleText(QString("时间T(x10^-11 s)"));
    }
}

void MainWindow::gainIndex(int index)
{
    if (index == 0) {
        ui->eleScence->data.clear();
        ui->eleScence->LineUp = (ui->up->isChecked())? true : false ;
        ui->eleScence->vs = sciNotation(ui->speedSize->text());
        ui->eleScence->vd = sciNotation(ui->speedDirection->text());
        ui->eleScence->cmr = sciNotation(ui->CMR->text());
        ui->eleScence->e = sciNotation(ui->eleSize->text());
        ui->eleScence->isUp = (ui->up->isChecked())? true : false ;
        ui->eleScence->isPost = (ui->isPosCharge->isChecked()) ? true : false;
        ui->eleScence->times = 0;
        ui->eleScence->dt = 0;
        ui->eleScence->calcPoints(-70, 0, -70, 70, -70, 70);
    } else if (index == 1) {
        ui->magScence->data.clear();
        ui->magScence->isInward = (ui->isOut->isChecked())? false : true ;
        ui->magScence->vs = sciNotation(ui->speedSize->text());
        ui->magScence->vd = sciNotation(ui->speedDirection->text());
        ui->magScence->cmr = sciNotation(ui->CMR->text());
        ui->magScence->b = sciNotation(ui->magSize->text());
        ui->magScence->isOut = (ui->isOut->isChecked())? true : false ;
        ui->magScence->isPost = (ui->isPosCharge->isChecked()) ? true : false;
        ui->magScence->times = 0;
        ui->magScence->dt = 0;
        ui->magScence->calcPoints(0, 0, -78, 78, -90, 90);

    } else {
        ui->eleAndMagScence->data.clear();
        ui->eleAndMagScence->isInward = (ui->isOut->isChecked())? false : true ;
        ui->eleAndMagScence->vs = sciNotation(ui->speedSize->text());
        ui->eleAndMagScence->vd = sciNotation(ui->speedDirection->text());
        ui->eleAndMagScence->cmr = sciNotation(ui->CMR->text());
        ui->eleAndMagScence->b = sciNotation(ui->magSize->text());
        ui->eleAndMagScence->e = sciNotation(ui->eleSize->text());
        ui->eleAndMagScence->isUp = (ui->up->isChecked())? true : false ;
        ui->eleAndMagScence->isOut = (ui->isOut->isChecked())? true : false ;
        ui->eleAndMagScence->isPost = (ui->isPosCharge->isChecked()) ? true : false;
        ui->eleAndMagScence->times = 0;
        ui->eleAndMagScence->dt = 0;
        ui->eleAndMagScence->calcPoints(0, 0, -81, 90, -100, 100);
    }


}


//开始演示的槽函数
void MainWindow::on_actStart_triggered(bool checked)
{
    //为图表设置标志
    myChartOne->flag = ui->plottingArea->currentIndex();
    myChartTwo->flag = ui->plottingArea->currentIndex();

    //为各个场景设置配置参数
    setScenceParameter(ui->plottingArea->currentIndex());

    //当点击开始演示按钮时，把面板设置全部禁掉
    disableAlls(false);

    //当点击开始演示时禁用或者打开一些工具栏按钮
    ui->actEle->setEnabled(false);
    ui->actMag->setEnabled(false);
    ui->actnEleMag->setEnabled(false);
    ui->actStop->setEnabled(true);
    ui->actStart->setEnabled(false);
    ui->actAgain->setEnabled(true);

    gainIndex(ui->plottingArea->currentIndex());
    updateAxis(ui->plottingArea->currentIndex());

    timer->start();
    timer->setInterval (50);    //设置定时周期


    //当点击开始演示后，设置电场场景中动作位为true，画背景的isMoing
    ui->eleScence->isMoving = true;

    //当点击开始演示后，设置电磁场场景中动作位为true，画背景的isMoing
    ui->eleAndMagScence->isMoving = true;
}

//停止演示的槽函数
void MainWindow::on_actStop_triggered()
{
    ui->actStop->setEnabled(false);
    ui->actGo->setEnabled(true);
    ui->eleScence->isDraw = false;//锁住
    ui->magScence->isDraw = false;
    timer->stop();
}

//继续演示的槽函数
void MainWindow::on_actGo_triggered()
{
    ui->actStop->setEnabled(true);
    timer->start();
    timer->setInterval(50);    //设置定时周期
}


//电场场景的槽函数
void MainWindow::on_actEle_triggered(bool checked)
{
    //清除图表区
    myChartOne->series0->clear();
    myChartOne->series1->clear();
    myChartTwo->series0->clear();
    myChartTwo->series1->clear();

    //隐藏右侧坐标轴
    myChartOne->axisYR->hide();
    myChartOne->chart->legend()->hide();
    myChartTwo->chart->legend()->hide();

    //更新ChartOne
    myChartOne->chart->setTitle("电场中粒子运动V-T示意图");
    myChartOne->axisY->setTitleText("速率V(x10^7 m/s)");

    //更新ChartTwo
    myChartTwo->chart->setTitle("电场中粒子运动θ-T示意图");
    myChartTwo->axisY->setTitleText("角度θ(°)");

    //切换至对应的tab页
    ui->plottingArea->setCurrentIndex(0);
    ui->valueDisplay->setCurrentIndex(0);

    //当点击电场时打开电场配置关闭磁场配置
    ui->eleSize->setEnabled(true);
    ui->up->setEnabled(true);
    ui->down->setEnabled(true);
    ui->magSize->setEnabled(false);
    ui->isInward->setEnabled(false);
    ui->isOut->setEnabled(false);

    //禁用||打开一些按钮
    ui->actEle->setEnabled(false);
    ui->actMag->setEnabled(true);
    ui->actnEleMag->setEnabled(true);
    disableButton();

    //点击电场后，赋予电场一个初始状态
    ui->up->click();
    ui->eleScence->LineUp = true;
    ui->speedSize->setText(QString("3.0x10^7"));
    ui->speedDirection->setText(QString("0"));
    ui->isNagCharge->setChecked(true);
    ui->CMR->setText("1.758x10^11");
    ui->eleSize->setText("1.0x10^4");
    ui->up->setChecked(true);

    //点击电场后，设置电磁场场景中动作位为false
    ui->eleAndMagScence->isMoving = false;

    ui->speedDirection->setEnabled(true);
    ui->speedDirection->setText("0");
    ui->speedDirection->setValidator(new QIntValidator(-90, 90, this));
}

//磁场场景的槽函数
void MainWindow::on_actMag_triggered(bool checked)
{
    //设置磁场方向默认为垂直纸面向里
    ui->isInward->setChecked(true);

    //清除图表区
    myChartOne->series0->clear();
    myChartOne->series1->clear();
    myChartTwo->series0->clear();
    myChartTwo->series1->clear();

    //隐藏右侧坐标轴
    myChartOne->axisYR->hide();
    myChartOne->chart->legend()->hide();
    myChartTwo->chart->legend()->hide();


    //更新ChartTwo
    myChartTwo->chart->setTitle("磁场中粒子运动θ-T示意图");
    myChartTwo->axisY->setTitleText("角度θ(°)");

    //切换至对应的tab页
    ui->plottingArea->setCurrentIndex(1);
    ui->valueDisplay->setCurrentIndex(1);

    //当点击磁场时打开磁场配置关闭电场配置
    ui->magSize->setEnabled(true);
    ui->isInward->setEnabled(true);
    ui->isOut->setEnabled(true);
    ui->eleSize->setEnabled(false);
    ui->up->setEnabled(false);
    ui->down->setEnabled(false);

    //禁用||打开一些按钮
    ui->actEle->setEnabled(true);
    ui->actMag->setEnabled(false);
    ui->actnEleMag->setEnabled(true);
    disableButton();

    //点击磁场后，赋予磁场一个初始状态
    ui->isInward->click();
    ui->magScence->isInward = true;
    ui->CMR->setText("0.4819x10^8");//0.4819 * 1e8
    ui->speedSize->setText("3.2x10^8");//3.2 * 1e6
    ui->isNagCharge->click();
    ui->magSize->setText("0.332");

    //更新ChartOne
    myChartOne->chart->setTitle("磁场中粒子运动V-T示意图");
    myChartOne->axisY->setTitleText(QString("速率V(x10^%1 m/s)")
    .arg(getNumLayer(sciResult(sciNotation(ui->speedSize->text())))));

    //点击磁场后，设置电场场景中动作位为false
    ui->eleScence->isMoving = false;

    //点击磁场后，设置电磁场场景中动作位为false
    ui->eleAndMagScence->isMoving = false;

    ui->speedDirection->setEnabled(true);
    ui->speedDirection->setText("0");

    ui->speedDirection->setValidator(new QIntValidator(-180, 180, this));
}

//电磁场场景的槽函数
void MainWindow::on_actnEleMag_triggered(bool checked)
{
    //清除图表区
    myChartOne->series0->clear();
    myChartOne->series1->clear();
    myChartTwo->series0->clear();
    myChartTwo->series1->clear();

    //显示出右侧坐标轴
    myChartOne->axisYR->show();
    myChartOne->series0->setName("V-T");
    myChartOne->series1->setName("θ-T");
    myChartTwo->series0->setName("Vx-T");
    myChartTwo->series1->setName("Vy-T");
    myChartOne->chart->legend()->show();
    myChartTwo->chart->legend()->show();

    //更新ChartOne
    myChartOne->chart->setTitle("电磁场中粒子运动V/θ-T示意图");

    myChartOne->axisY->setTitleText(QString("速率V(x10^%1 m/s)")
    .arg(getNumLayer(sciResult(sciNotation(ui->speedSize->text())))));
    myChartOne->axisYR->setTitleText("角度θ(°)");

    //更新ChartTwo
    myChartTwo->chart->setTitle("电磁场中粒子运动Vx/Vy-T示意图");
    myChartTwo->axisY->setTitleText(QString("速率Vx/Vy(x10^%1 m/s)")
    .arg(getNumLayer(sciResult(sciNotation(ui->speedSize->text()))) - 2));

    //切换至对应的tab页
    ui->plottingArea->setCurrentIndex(2);
    ui->valueDisplay->setCurrentIndex(2);

    //当点击电磁场时打开电磁场的配置
    ui->magSize->setEnabled(true);
    ui->eleSize->setEnabled(true);
    ui->speedDirection->setEnabled(false);
    ui->up->setEnabled(true);
    ui->down->setEnabled(true);
    ui->isInward->setEnabled(true);
    ui->isOut->setEnabled(true);

    //禁用||打开一些按钮
    ui->actEle->setEnabled(true);
    ui->actMag->setEnabled(true);
    ui->actnEleMag->setEnabled(false);
    disableButton();

    //点击电磁场后，赋予电磁场一个初始状态
    ui->isInward->click();
    ui->eleAndMagScence->isInward = true;
    ui->eleAndMagScence->LineUp = true;
    ui->eleAndMagScence->repaint();

    ui->isInward->click();
    ui->magSize->setText("0.06");
    ui->speedSize->setText(QString("3.0x10^6"));
    ui->speedDirection->setText(QString("0"));
    ui->isNagCharge->setChecked(true);
    ui->CMR->setText("1.758x10^11");
    ui->eleSize->setText("9.0x10^4");
    ui->up->setChecked(true);

    //点击电磁场后，设置电场场景中动作位为false
    ui->eleScence->isMoving = false;

}

//重新演示的槽函数
void MainWindow::on_actAgain_triggered(bool checked)
{
    //开启对应的面板设置
    openSomeSettings(ui->plottingArea->currentIndex());

    //显示数据框置0
    setZeroAndNoenable("0", false); //置0和禁止输入

    //时间t归0
    dt = 0;
    myChartOne->dt = 0;
    myChartTwo->dt = 0;

    //清楚绘图区
    clearPlottingArea();

    //清楚图表区
    myChartOne->series0->clear();
    myChartOne->series1->clear();
    myChartTwo->series1->clear();
    myChartTwo->series0->clear();

    //关闭定时器
    timer->stop();

    //禁用||打开一些按钮
    ui->actEle->setEnabled(true);
    ui->actMag->setEnabled(true);
    ui->actnEleMag->setEnabled(true);
    ui->actAgain->setEnabled(false);
    ui->actStart->setEnabled(true);
    ui->actStop->setEnabled(false);
    ui->actGo->setEnabled(false);

    if(ui->plottingArea->currentIndex() == 2)
    {
        ui->speedDirection->setEnabled(false);
    }
}

void MainWindow::on_isInward_clicked(bool checked)
{//磁场方向曹函数，里
    if (ui->plottingArea->currentIndex() == 1) {//如果在磁场中
        ui->magScence->isInward = true;
        ui->magScence->isOut = false;
        ui->magScence->repaint();
    } else {
        ui->eleAndMagScence->isInward = true;
        ui->eleAndMagScence->repaint();
    }
}

void MainWindow::on_isOut_clicked(bool checked)
{//磁场方向曹函数，外
    if (ui->plottingArea->currentIndex() == 1) {//如果在磁场中
        ui->magScence->isInward = false;
        ui->magScence->isOut = true;
        ui->magScence->repaint();
    } else {
        ui->eleAndMagScence->isInward = false;
        ui->eleAndMagScence->repaint();
    }

}

void MainWindow::on_down_clicked()
{//电场方向曹函数，向下
    if (ui->plottingArea->currentIndex() == 0) {
        ui->eleScence->LineUp = false;
        ui->eleScence->isUp = false;
        ui->eleScence->repaint();
    } else {
        ui->eleAndMagScence->LineUp = false;
        ui->eleAndMagScence->repaint();
    }
}


void MainWindow::on_up_clicked()
{//电场方向曹函数，向上
    if (ui->plottingArea->currentIndex() == 0) {
        ui->eleScence->LineUp = true;
        ui->eleScence->isUp = true;
        ui->eleScence->repaint();
    } else {
        ui->eleAndMagScence->LineUp = true;
        ui->eleAndMagScence->repaint();
    }
}


void MainWindow::on_isPosCharge_clicked(bool checked)
{//电性曹函数，正电

    if (ui->plottingArea->currentIndex() == 0) {//如果在电场中
        ui->CMR->setText("1.758x10^11");
        ui->eleScence->isPost = true;
        ui->eleScence->repaint();
    }
    else if (ui->plottingArea->currentIndex() == 1) {
        ui->CMR->setText("0.4819x10^8");//0.4819 * 1e8
        ui->magScence->isPost = true;
        ui->magScence->repaint();
    } else {
        ui->CMR->setText("1.758x10^11");
        ui->eleAndMagScence->isPost = true;
        ui->eleAndMagScence->repaint();
    }
}

void MainWindow::on_isNagCharge_clicked(bool checked)
{//电性曹函数，负电
    if (ui->plottingArea->currentIndex() == 0) {//如果在电场中
        ui->CMR->setText("1.758x10^11");
        ui->eleScence->isPost = false;
        ui->eleScence->repaint();
    }
    else if (ui->plottingArea->currentIndex() == 1) {
        ui->CMR->setText("0.4819x10^8");//0.4819 * 1e8
        ui->magScence->isPost = false;
        ui->magScence->repaint();
    } else {
        ui->CMR->setText("1.758x10^11");
        ui->eleAndMagScence->isPost = false;;
        ui->eleAndMagScence->repaint();
    }
}

void MainWindow::on_speedSize_textChanged(const QString &arg1)
{
    QStringList complete_list;
    QCompleter* complete_lineEdit;
    if(ui->plottingArea->currentIndex() == 0)
    {
        complete_list<<arg1+"x10^7";
    }
    else if (ui->plottingArea->currentIndex() == 1)
    {
        complete_list<<arg1+"x10^8";
    }
    else
    {
        complete_list<<arg1+"x10^6";
    }

    complete_lineEdit=new QCompleter(complete_list);
    ui->speedSize->setCompleter(complete_lineEdit);
}

void MainWindow::on_CMR_textChanged(const QString &arg1)
{
    QStringList complete_list;
    QCompleter* complete_lineEdit;
    if(ui->plottingArea->currentIndex() == 0)
    {
        complete_list<<arg1+"x10^11";
    }
    else if (ui->plottingArea->currentIndex() == 1)
    {
        complete_list<<arg1+"x10^8";
    }
    else
    {
        complete_list<<arg1+"x10^11";
    }
    complete_lineEdit=new QCompleter(complete_list);
    ui->CMR->setCompleter(complete_lineEdit);
}

void MainWindow::on_eleSize_textChanged(const QString &arg1)
{
    QStringList complete_list;
    QCompleter* complete_lineEdit;
    complete_list<<arg1+"x10^4";
    complete_lineEdit=new QCompleter(complete_list);
    ui->eleSize->setCompleter(complete_lineEdit);
}

void MainWindow::on_speedSize_editingFinished()
{
    if(ui->plottingArea->currentIndex() == 1)
    {
        myChartOne->axisY->setRange(0, 2 * (sciNotation(sciResult
        (sciNotation(ui->speedSize->text())))) /
        pow(10, getNumLayer(sciResult(sciNotation(ui->speedSize->text())))));

        myChartOne->axisY->setTitleText(QString("速率V(x10^%1m/s)")
        .arg(getNumLayer(sciResult(sciNotation(ui->speedSize->text())))));
    }
}
