#include "MyChartOne.h"
#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "Calculate.h"
#include <QDebug>

MyChartOne::MyChartOne()
{
    initDataMem();  //初始化数据成员
    initChart();    //初始化图表
}

void MyChartOne::updateChart()
{
    dt += 1;
    if(flag == 0)
    {

        double V = inEleCalV(pow(10, -9.55) * dt, Vs, Vd, CMR, isPos, E, isUp) / pow(10, layer);
        series0->append(dt, V);

    }
    else if(flag == 1)
    {
        axisY->setRange(0, 2 * (sciNotation(sciResult(Vs))) /
                       pow(10, getNumLayer(sciResult(Vs))));

        double V = Vs / pow(10, getNumLayer(sciResult(Vs)));

        series0->append(dt, V);
    }
    else
    {


        double V = inEleAndMagCalV(dt * pow(10, -11), Vs, CMR, isPos, E, isUp, B, isOut) /
                   pow(10, this->layer);
        series0->append(dt, V);

        double A = inEleAndMagCalA(dt * pow(10, -11), Vs, CMR, isPos, E, isUp, B, isOut);
        series1->append(dt, A);

    }
}

void MyChartOne::initChart()
{
    //设置图表标题并隐藏legend
    chart->setTitle("电场中粒子运动V-T示意图");
    QFont fonttitle;
    fonttitle.setFamily("隶书");
    //fonttitle.setPointSize(12);
    fonttitle.setPixelSize(24);
    chart->setTitleFont(fonttitle);
    chart->legend()->hide();

    //设置坐标字体
    QFont font;
    font.setFamily("黑体");
    font.setPointSize(9);
    font.setPixelSize(18);

    //配置各个坐标轴
    axisX->setRange(0, 78);
    axisX->setTickCount(5);
    axisX->setLabelsFont(font);
    axisX->setTitleText("时间T(x10^-9.5 s)");
    axisX->setTitleFont(font);

    axisY->setRange(0, 7.1);
    axisY->setTickCount(5);
    axisY->setLabelsFont(font);
    axisY->setTitleText("速率V(x10^7 m/s)");
    axisY->setTitleFont(font);

    axisYR->setRange(-180, 180);
    axisYR->setTickCount(5);
    axisYR->setLabelsFont(font);
    axisYR->setTitleText("角度θ(°)");
    axisYR->setTitleFont(font);

    //将各个坐标轴添加到图表上
    chart->addAxis(axisX, Qt::AlignBottom);          // 将X轴添加到图表上
    chart->addAxis(axisY, Qt::AlignLeft);            // 将Y轴添加到图表上
    chart->addAxis(axisYR, Qt::AlignRight);          // 将右Y轴添加到图表上

    //将曲线序列添加到图表上
    chart->addSeries(series0);                        // 将曲线对象添加到图表上
    chart->addSeries(series1);                       // 添加曲线0

    //为图表设置动画
    chart->setAnimationOptions(QChart::SeriesAnimations);

    //将曲线序列和对应的坐标轴关联
    series0->attachAxis(axisX);
    series1->attachAxis(axisX);
    series0->attachAxis(axisY);
    series1->attachAxis(axisYR);

    //设置序列点可见
    series0->setPointsVisible(true);
    series1->setPointsVisible(true);

    //默认隐藏掉右侧坐标轴
    axisYR->hide();
}

void MyChartOne::initDataMem()
{
    chart = new QChart();              //创建图表
    axisX = new QValueAxis();          //创建X轴
    axisY = new QValueAxis();          //创建Y轴
    axisYR= new QValueAxis();          //创建右Y轴
    series0 = new QSplineSeries();       //创建序列线0
    series1 = new QSplineSeries();       //创建序列线1
}




