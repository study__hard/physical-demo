#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts>
QT_CHARTS_USE_NAMESPACE
#include "MyChartOne.h"
#include "MyChartTwo.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

private slots:
    void on_actStart_triggered(bool checked);

    void on_actStop_triggered();

    void on_actGo_triggered();

    void on_actEle_triggered(bool checked);

    void on_actMag_triggered(bool checked);

    void on_actnEleMag_triggered(bool checked);

    void on_actAgain_triggered(bool checked);

    void on_isInward_clicked(bool checked);

    void on_isOut_clicked(bool checked);

    void on_down_clicked();

    void on_up_clicked();

    void on_isPosCharge_clicked(bool checked);

    void on_isNagCharge_clicked(bool checked);

    void on_speedSize_textChanged(const QString &arg1);

    void on_CMR_textChanged(const QString &arg1);

    void on_eleSize_textChanged(const QString &arg1);

    void on_speedSize_editingFinished();

private:
    QTimer* timer;                                   //计时器
    MyChartOne* myChartOne;                          //图表一
    MyChartTwo* myChartTwo;                          //图表二
    Ui::MainWindow* ui;                              //指向ui界面的指针
    double dt = 0.0;                                 //读秒

private:
    void initUI();                                   //初始化UI界面
    void initDataMem();                              //初始化数据成员
    void initConnect();                              //初始化conne连接
    void initParameterSet();                         //初始化面板参数设置
    void setScenceParameter(int index);              //为各个场景设置配置参数
    void defaultIntoEle();                           //默认进入电场
    void disableButton();                            //禁用一些按钮
    void disableAlls(bool flag);                     //禁用面板设置
    void openSomeSettings(int index);                //开启部分面板设置
    void clearPlottingArea();                        //当点击重新演示按钮时清楚数据重绘界面
    void setValue(double dt, int index);             //实时显示数据
    void setZeroAndNoenable(QString str, bool flag); //开始把所有显示数据框置0

    void emitAgain();
    void updateAxis(int index);
    void gainIndex(int index);

};

#endif // MAINWINDOW_H
