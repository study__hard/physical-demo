#ifndef MYCHART_H
#define MYCHART_H

#include <QtCharts>
QT_CHARTS_USE_NAMESPACE


class MyChartOne : public QChartView
{
public:
    MyChartOne();

public:
    QChart *chart;                   //图表的画布
    QLineSeries *series0;            //序列线0
    QLineSeries *series1;            //序列线1
    QValueAxis *axisX;               //X轴
    QValueAxis *axisY;               //Y轴
    QValueAxis* axisYR;              //右Y轴
    int flag = 0;                    //标识场景

    double dt = 0;
    double Vs = 0.0;
    double Vd = 0.0;
    double CMR = 0.0;
    bool isPos = true;
    double E = 0.0;
    bool isUp = true;
    double B = 0.0;
    bool isOut = true;
    double size = 0.0;
    double layer = 0.0;
public:
    void updateChart();             //更新序列线的数据

private:
    void initChart();               //初始化图表
    void initDataMem();             //初始化数据成员
};

#endif // MYCHART_H
