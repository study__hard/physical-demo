#ifndef MAGSCENCE_H
#define MAGSCENCE_H

#include <QWidget>

class MagScence : public QWidget
{
    Q_OBJECT
public:
    explicit MagScence(QWidget *parent = nullptr);

protected:
    //绘制
    void paintEvent(QPaintEvent *event) override;

private:
    void myDrawTitle(QPainter& painter);                              //画场景标题
    void myDrawBackground(QPainter& painter);                         //画场景背景
    void mySetWindow(QPainter& painter, int x, int y, int w, int h);  //定义视口

public:
    void myDrawPath(QPainter& painter, qreal top, qreal bottom, qreal left, qreal right);        //绘制粒子轨迹
    void calcPoints(qreal startX, qreal startY, qreal top, qreal bottom, qreal left, qreal right);//算点

public:
    bool isDraw;                       //是否追加数据的标志
    bool isInward;                     //绘制磁场方向的标志
    double dt;
    QVector<QPointF> data;
public:
    double vs;
    double vd;
    double cmr;
    double isPost;
    double b;
    double isOut;
    int times;
    bool isRepaint = false;


signals:

public slots:
};


#endif // MAGSCENCE_H
