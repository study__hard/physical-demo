#include "MagScence.h"
#include <cmath>
#include <QtMath>
#include <QGuiApplication>
#include <QResizeEvent>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QPaintEvent>
#include <QPainter>
#include <QPainterPath>
#include <Calculate.h>

MagScence::MagScence(QWidget *parent) : QWidget(parent)
{
    isInward = true;
    isDraw = false;

    //初始状态设置
    vs = 3.2 * 1e8;
    vd = 0;
    cmr = 0.4819 * 1e8;
    isPost = true;
    isOut = false;
    times = 0;
    b = 0.332;
    dt = 0;
    calcPoints(0, 0, -78, 78, -90, 90);

}

void MagScence::paintEvent(QPaintEvent *event)
{

    event->accept();
    QPainter painter(this);

    //设置抗锯齿
    painter.setRenderHint(QPainter::Antialiasing);

    //设置窗口
    mySetWindow(painter, 100, 100, 200, 200);

    //绘制标题
    myDrawTitle(painter);

    //绘制背景
    myDrawBackground(painter);

    //绘制粒子
    myDrawPath(painter, -78, 78, -90, 90);
    //绘制粒子轨迹
    if (times >= 2) {
        for (int i = 2; i < times; ++i) {
            painter.drawPoint(data[i - 2]);
        }
    }
    this->isDraw = false;
}

//画场景标题
void MagScence::myDrawTitle(QPainter &painter)
{

    QFont font;
    font.setFamily("隶书");
    font.setPointSize(8);
    font.setPixelSize(12);
    painter.setFont(font);
    painter.setPen(QColor(235,51,36));
    painter.drawText(QRectF(-80,-110,160,40),Qt::AlignCenter, "粒子在磁场中的运动");

}

void MagScence::myDrawBackground(QPainter &painter)
{
    QPen pen;

    //画坐标轴
    if (true) {
        pen.setWidth(0);
        pen.setStyle(Qt::SolidLine);
        QColor color;
        color.setAlpha(50); //设置背景透明度
        pen.setColor(color);
        painter.setPen(pen);
        int x1 = 0;
        int x2 = 0;
        int y1 = 78;
        int y2 = -76;
        painter.drawLine(x1, y1, x2, y2);//箭身
        double arrowEndX1, arrowEndY1;//箭头端点1
        double arrowEndX2, arrowEndY2;//箭头端点2
        double arrowLength = 5;       // 箭头长度
        arrowEndX1 = x2 - arrowLength * 0.5;
        arrowEndY1 = y2 + arrowLength * sqrt(3)/2;
        arrowEndX2 = x2 + arrowLength * 0.5;
        arrowEndY2 = y2 + arrowLength * sqrt(3)/2;
        painter.drawLine(x2, y2, arrowEndX1, arrowEndY1);//画箭头的第一个角
        painter.drawLine(x2, y2, arrowEndX2, arrowEndY2);//画箭头的第二个角
        QFont font;
        font.setPointSize(1);
        font.setPixelSize(5);
        painter.setFont(font);
        painter.drawText(QRectF(4, -76, 10, 10),Qt::AlignLeft, "y");
        x1 = -86;
        x2 = 85;
        y1 = 0;
        y2 = 0;
        painter.drawLine(x1, y1, x2, y2);//箭身
        arrowEndX1 = x2 - arrowLength * sqrt(3)/2;
        arrowEndY1 = y2 + arrowLength * 0.5;
        arrowEndX2 = x2 - arrowLength * sqrt(3)/2;
        arrowEndY2 = y2 - arrowLength * 0.5;
        painter.drawLine(x2, y2, arrowEndX1, arrowEndY1);//画箭头的第一个角
        painter.drawLine(x2, y2, arrowEndX2, arrowEndY2);//画箭头的第二个角
        painter.drawText(QRectF(85, 2, 10, 10),Qt::AlignLeft, "x");
    } else {
        pen.setWidth(0);
        pen.setStyle(Qt::SolidLine);
        painter.setPen(pen);
        int x1 = 0;
        int x2 = 0;
        int y1 = 78;
        int y2 = -76;
        painter.drawLine(x1, y1, x2, y2);//箭身
        double arrowEndX1, arrowEndY1;//箭头端点1
        double arrowEndX2, arrowEndY2;//箭头端点2
        double arrowLength = 5;       // 箭头长度
        arrowEndX1 = x2 - arrowLength * 0.5;
        arrowEndY1 = y2 + arrowLength * sqrt(3)/2;
        arrowEndX2 = x2 + arrowLength * 0.5;
        arrowEndY2 = y2 + arrowLength * sqrt(3)/2;
        painter.drawLine(x2, y2, arrowEndX1, arrowEndY1);//画箭头的第一个角
        painter.drawLine(x2, y2, arrowEndX2, arrowEndY2);//画箭头的第二个角
        QFont font;
        font.setPointSize(1);
        font.setPixelSize(5);
        painter.setFont(font);
        painter.drawText(QRectF(4, -76, 10, 10),Qt::AlignLeft, "y");
        x1 = -86;
        x2 = 85;
        y1 = 0;
        y2 = 0;
        painter.drawLine(x1, y1, x2, y2);//箭身
        arrowEndX1 = x2 - arrowLength * sqrt(3)/2;
        arrowEndY1 = y2 + arrowLength * 0.5;
        arrowEndX2 = x2 - arrowLength * sqrt(3)/2;
        arrowEndY2 = y2 - arrowLength * 0.5;
        painter.drawLine(x2, y2, arrowEndX1, arrowEndY1);//画箭头的第一个角
        painter.drawLine(x2, y2, arrowEndX2, arrowEndY2);//画箭头的第二个角
        painter.drawText(QRectF(85, 2, 10, 10),Qt::AlignLeft, "x");
    }

    //恢复画笔

    pen.setWidth(0);
    pen.setColor(QColor(0,0,0));
    pen.setStyle(Qt::DashLine);
    painter.setPen(pen);

    //画笔
    painter.drawRect(-90, -78, 180, 160);
    int cx,cy;
    for(cx= -90; cx<= 75; cx++)
    {
        cx+=7;
        //if(cx%3==0)
        for(cy= -78; cy<= 70;cy++)
        {
            cy+=9;
            pen.setWidth(0);
            pen.setStyle(Qt::SolidLine);
            QColor color;
            color.setAlpha(50); //设置背景透明度
            pen.setColor(color);
            painter.setPen(pen);
            //垂直纸面向内
            if(isInward)
            {
                color.setAlpha(100);//设置画笔透明度
                pen.setColor(color);
                painter.setPen(pen);
                painter.drawLine(QPoint(cx, cy), QPoint(cx+3, cy+3));
                painter.drawLine(QPoint(cx+3, cy), QPoint(cx, cy+3));
            }
            //垂直纸面向外
            else
            {
                pen.setWidth(1);
                pen.setStyle(Qt::SolidLine);
                QColor color;
                color.setAlpha(50); //设置背景透明度
                pen.setColor(color);
                painter.setPen(pen);
                painter.drawPoint(cx,cy);
            }

        }
    }
}

void MagScence::myDrawPath(QPainter &painter, qreal top, qreal bottom, qreal left, qreal right)
{
    QPen pen;
    pen.setWidthF(0.8);
    painter.setPen(pen);
    if (this->isDraw) {
        if (this->isPost) {//如果是正电小球
            if (times < data.size() &&  data[times].x() >= left && data[times].x() <= right
                    && data[times].y() <= bottom && data[times].y() >= top) {
                painter.drawLine(QPointF(data[times].x() - 1, data[times].y()), QPointF(data[times].x() + 1, data[times].y()));
                painter.drawLine(QPointF(data[times].x(), data[times].y() + 1), QPointF(data[times].x(), data[times].y() - 1));
                painter.drawEllipse(data[times], 2, 2);
                times++;
            } else {
                isRepaint = true;
                times = 0;
                isDraw = false;
            }
        } else {//如果是负电小球
            if (times < data.size() &&  data[times].x() >= left && data[times].x() <= right
                    && data[times].y() <= bottom && data[times].y() >= top) {
                painter.drawLine(QPointF(data[times].x() - 1, data[times].y()), QPointF(data[times].x() + 1, data[times].y()));
                painter.drawEllipse(data[times], 2, 2);
                times++;
            } else {
                isRepaint = true;
                times = 0;
                isDraw = false;
            }
        }
    } else {
        if (times >= 1) {//如果粒子已经在运动,该if分支防止暂停时，拖拉窗口重绘导致的小球消失问题
            if (this->isPost) {//如果是正电小球
                if (times - 1 < data.size() &&  data[times - 1].x() >= left && data[times - 1].x() <= right
                        && data[times - 1].y() <= bottom && data[times - 1].y() >= top) {
                    painter.drawLine(QPointF(data[times - 1].x() - 1, data[times - 1].y()), QPointF(data[times -1].x() + 1, data[times - 1].y()));
                    painter.drawLine(QPointF(data[times - 1].x(), data[times - 1].y() + 1), QPointF(data[times - 1].x(), data[times - 1].y() - 1));
                    painter.drawEllipse(data[times - 1], 2, 2);
                } else {
                    times = 0;
                    isDraw = false;
                }
            } else {//如果是负电小球
                if (times - 1 < data.size() &&  data[times - 1].x() >= left && data[times - 1].x() <= right
                        && data[times - 1].y() <= bottom && data[times - 1].y() >= top) {
                    painter.drawLine(QPointF(data[times - 1].x() - 1, data[times - 1].y()), QPointF(data[times - 1].x() + 1, data[times - 1].y()));
                    painter.drawEllipse(data[times - 1], 2, 2);
                } else {
                    times = 0;
                    isDraw = false;
                }
            }
        } else {//绘制初始位置的小球
            if (this->isPost) {//如果是正电小球
                painter.drawLine(QPointF(- 1, 0), QPointF( 1, 0));
                painter.drawLine(QPointF(0,  1), QPointF(0, - 1));
                painter.drawEllipse(QPointF(0, 0), 2, 2);
                isDraw = false;
            } else {//如果是负电小球
                painter.drawLine(QPointF(- 1, 0), QPointF( 1, 0));
                painter.drawEllipse(QPointF(0, 0), 2, 2);
                isDraw = false;
            }
        }
    }

}
void MagScence::calcPoints(qreal startX, qreal startY, qreal top, qreal bottom, qreal left, qreal right)
{
    /* *
     *  double vs;
     *  double vd;
     *  double cmr;
     *  double isPost;
     *  double b;
     *  double isOut;
     *
     * */
    QPointF begin = {startX, startY};
    data.push_back(begin);
    QPointF particle = begin;

    while (particle.x() >= left && particle.x() <= right && particle.y() <= bottom && particle.y() >= top) {
        dt += 1;
        particle.setX(inMagCalX(pow(10, -8.5) * dt, this->vs, this->vd, this->cmr, this->isPost, this->b, this->isOut));
        particle.setY((-inMagCalY(pow(10, -8.5) * dt, this->vs, this->vd, this->cmr, this->isPost, this->b, this->isOut)));
        data.push_back(particle);

        if (dt * pow(10, -8.5) * cmr * b >= 2 * M_PI) {
            break;
        }
    }

}
void MagScence::mySetWindow(QPainter &painter, int x, int y, int w, int h)
{
    int W=width();
    int H=height();

    //取长和宽的小值
    int side=qMin(W,H);

    //viewport矩形区
    QRect rect((W-side)/2, (H-side)/2,side,side);
    //painter.drawRect(rect);


    //设置Viewport
    painter.setViewport(rect);

    //设置窗口大小，逻辑坐标
    painter.setWindow(-x, -y, w, h);

}
