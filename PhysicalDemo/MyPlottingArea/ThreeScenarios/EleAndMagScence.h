#ifndef ELEANDMAGSCENCE_H
#define ELEANDMAGSCENCE_H

#include <QWidget>

class EleAndMagScence : public QWidget
{
    Q_OBJECT
public:
    explicit EleAndMagScence(QWidget *parent = nullptr);

protected:
    //绘制
    void paintEvent(QPaintEvent *event) override;


private:
    void myDraw(QPainter& painter);
    void mySetWindow(QPainter& painter, int x, int y, int w, int h);
    void m_paintMag(QPainter& painter);
    void m_paintEle(QPainter& painter);
    void drawArrow(double x1, double y1, double x2, double y2, QPainter& painter);
public:
    void myDrawPath(QPainter& painter, qreal top, qreal bottom, qreal left, qreal right);        //绘制粒子轨迹
    void calcPoints(qreal startX, qreal startY, qreal top, qreal bottom, qreal left, qreal right);//算点
public:
    bool isDraw;
    bool isInward;
    bool isMoving = false;
    bool LineUp;
    double dt;
    QVector<QPointF> data;
private:

public:
    double vs;
    double vd;
    double cmr;
    double isPost;
    double b;
    double isOut;
    double e;
    double isUp;
    int times;
    bool isRepaint = false;


signals:

public slots:

};

#endif // ELEANDMAGSCENCE_H
