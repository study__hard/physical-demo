#ifndef ELESCENCE_H
#define ELESCENCE_H

#include <QWidget>


class EleScence : public QWidget
{
    Q_OBJECT
public:
    explicit EleScence(QWidget *parent = nullptr);

protected:
    void paintEvent(QPaintEvent *event) override;

public:
    QVector<QPointF> data;
    bool LineUp;
    bool isMoving;
    double dt;                        //读秒
    bool isDraw;                      //是否重绘，加锁
    double vs;
    double vd;
    double cmr;
    double isPost;
    double e;
    double isUp;
    int times;
    bool isRepaint = false;
private:
    void myDraw(QPainter& painter);
    void mySetWindow(QPainter& painter, int x, int y, int w, int h);
    void drawArrow(double x1, double y1, double x2, double y2, QPainter& painter);
public:
    void myDrawPath(QPainter& painter, qreal top, qreal bottom, qreal left, qreal right);          //绘制粒子轨迹
    void calcPoints(qreal startX, qreal startY, qreal top, qreal bottom, qreal left, qreal right); //算点

signals:

public slots:
};

#endif // ELESCENCE_H

